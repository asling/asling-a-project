# a-project

#### 介绍
一个基于lerna的多依赖包管理脚手架，集成了eslint、prettier、单元测试和提交校验等功能。

#### 软件架构
```sh
.
└── packages
  ├── module A
  ├── module B
  .
  .
  .
└── lerna.json
└── .eslintrc.js
└── .eslintigore
└── .prettierrc.js
└── .prettierignore
└── package.json
└── README.md

```
#### 安装教程

1.  yarn run install
2.  yarn run bootstrap
3.  yarn run build

#### 使用说明

1.  提交代码
2.  执行yarn run publish完成依赖包发布

#### 参与贡献

1.  Fork 本仓库
2.  新建 feature/** 分支
3.  提交代码
4.  新建 Pull Request

