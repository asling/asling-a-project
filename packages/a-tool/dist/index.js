'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var _isArray = require('lodash/isArray');
var _isObject = require('lodash/isObject');
var _findIndex = require('lodash/findIndex');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var _isArray__default = /*#__PURE__*/_interopDefaultLegacy(_isArray);
var _isObject__default = /*#__PURE__*/_interopDefaultLegacy(_isObject);
var _findIndex__default = /*#__PURE__*/_interopDefaultLegacy(_findIndex);

const getKey = (arr, condition) => {
  if (!_isArray__default['default'](arr)) return undefined;
  if (!_isObject__default['default'](condition)) return undefined;
  const predicate = Object.keys(condition).reduce((prev, cur) => {
    return prev.concat([cur, condition[cur]]);
  }, []);
  return _findIndex__default['default'](arr, predicate);
};

const getItem = (arr, condition) => {
  const key = getKey(arr, condition);
  return key > -1 ? arr[key] : undefined;
};

const setItem = (arr, condition) => {
  const returnArr = Array.from(arr);
  const key = getKey(arr, condition);
  if (key > -1) {
    returnArr[key] = { ...arr[key], ...condition };
  } else {
    returnArr.push(condition);
  }
};

exports.getItem = getItem;
exports.setItem = setItem;
