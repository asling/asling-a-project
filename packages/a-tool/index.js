import _isArray from 'lodash/isArray';
import _isObject from 'lodash/isObject';
import _findIndex from 'lodash/findIndex';

const getKey = (arr, condition) => {
  if (!_isArray(arr)) return undefined;
  if (!_isObject(condition)) return undefined;
  const predicate = Object.keys(condition).reduce((prev, cur) => {
    return prev.concat([cur, condition[cur]]);
  }, []);
  return _findIndex(arr, predicate);
};

export const getItem = (arr, condition) => {
  const key = getKey(arr, condition);
  return key > -1 ? arr[key] : undefined;
};

export const setItem = (arr, condition) => {
  const returnArr = Array.from(arr);
  const key = getKey(arr, condition);
  if (key > -1) {
    returnArr[key] = { ...arr[key], ...condition };
  } else {
    returnArr.push(condition);
  }
};
