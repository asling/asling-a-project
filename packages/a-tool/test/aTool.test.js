const { expect } = require('chai');
const { getItem } = require('../dist/index');

describe('a-tool package unit test', () => {
  describe('#getItem(arr, condition)', () => {
    it('should return undefined when no input param', () => {
      expect(getItem()).to.equal(undefined);
    });

    it('should return undefined when typeof param `arr` is not Array', () => {
      expect(getItem({})).to.equal(undefined);
    });

    it('should return undefined when typeof param `condition` is not Object', () => {
      expect(getItem([1, 2, 3], {})).to.equal(undefined);
    });

    it('should return {id: 1, value: 1} when param `condition` is {id: 1, value: 1}', () => {
      const arr = [
        { id: 1, value: 1 },
        { id: 2, value: 2 },
        { id: 3, value: 3 },
      ];

      expect(getItem(arr, { id: 1, value: 1 })).to.deep.equal({
        id: 1,
        value: 1,
      });
    });
  });
});
