'use strict';

var aTool = require('@asling/a-tool');
var _isArray = require('lodash/isArray');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var _isArray__default = /*#__PURE__*/_interopDefaultLegacy(_isArray);

// const arr = [
//   { id: 1, value: 1 },
//   { id: 2, value: 2 },
//   { id: 3, value: 3 },
// ];

function run(arr) {
  if (!_isArray__default['default'](arr)) return false;
  const item = aTool.getItem(arr, { id: 1 });
  // todo
  // $("#root").val(item?.value);
  return item?.value;
}

module.exports = run;
