import { getItem } from '@asling/a-tool';
// import $ from "jquery";
import _isArray from 'lodash/isArray';

// const arr = [
//   { id: 1, value: 1 },
//   { id: 2, value: 2 },
//   { id: 3, value: 3 },
// ];

export default function run(arr) {
  if (!_isArray(arr)) return false;
  const item = getItem(arr, { id: 1 });
  // todo
  // $("#root").val(item?.value);
  return item?.value;
}
