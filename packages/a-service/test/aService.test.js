const { expect } = require('chai');
const { JSDOM } = require('jsdom');
const chai = require('chai');
const run = require('../dist/index');

chai.use(require('chai-dom'));

const dom = new JSDOM(`<!DOCTYPE html><html><body></body></html>`);
global.window = dom.window;
global.document = dom.document;

describe('aService unit test', () => {
  describe('#run()', () => {
    let root;
    beforeEach(() => {
      root = dom.window.document.createElement('div');
      root.setAttribute('id', 'root');
      dom.window.document.body.appendChild(root);
    });

    it('should have id `root`', () => {
      expect(dom.window.document.querySelector('div')).to.have.id('root');
    });

    it('should have text `1` ', () => {
      const arr = [
        { id: 1, value: 1 },
        { id: 2, value: 2 },
        { id: 3, value: 3 },
      ];
      const textNode = dom.window.document.createTextNode(run(arr));
      expect(dom.window.document.querySelector('div').appendChild(textNode)).to.have.text('1');
    });

    afterEach(() => {
      dom.window.document.body.removeChild(root);
      root = undefined;
    });
  });
});
